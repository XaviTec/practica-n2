import 'package:flutter/material.dart';

class IconsList {
  final IconData icon;
  final String name;
  IconsList({required this.icon, required this.name});
}
