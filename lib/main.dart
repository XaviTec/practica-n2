import 'package:flutter/material.dart';

import 'model/icons.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Emelec'),
          centerTitle: true,
        ),
        body: Column(
          children: const [
            _ImageContainer(),
            _Contenido(),
          ],
        ),
      ),
    );
  }
}

class _ImageContainer extends StatelessWidget {
  const _ImageContainer();

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/emelec.jpeg',
    );
  }
}

class _Contenido extends StatelessWidget {
  const _Contenido();

  @override
  Widget build(BuildContext context) {
    final List<IconsList> list = [
      IconsList(icon: Icons.call, name: 'CALL'),
      IconsList(icon: Icons.near_me, name: 'ROUTE'),
      IconsList(icon: Icons.share, name: 'SHARE'),
    ];

    return Column(
      children: [
        const _Title(
          title: 'Estadio George Capwell',
          subTitle: 'El Mítico y la Caldera.',
          populary: '5',
        ),
        _Icons(
          items: list,
        ),
        const _TextContainer(),
      ],
    );
  }
}

class _Title extends StatelessWidget {
  final String title;
  final String subTitle;
  final String populary;
  const _Title({
    required this.title,
    required this.subTitle,
    required this.populary,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    title,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  subTitle,
                  style: const TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
          /*3*/
          const Icon(
            Icons.star,
            color: Colors.red,
          ),
          Text(populary),
        ],
      ),
    );
  }
}

class _Icons extends StatelessWidget {
  const _Icons({
    required this.items,
  });
  final List<IconsList> items;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: items
          .map(
            (item) => _icon(item),
          )
          .toList(),
    );
  }

  Column _icon(IconsList item) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(item.icon, color: Colors.blue),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            item.name,
            style: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue,
            ),
          ),
        ),
      ],
    );
  }
}

class _TextContainer extends StatelessWidget {
  const _TextContainer();

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(32),
      child: Text(
        'El Estadio George Capwell es un estadio de fútbol ubicado en la calle San Martín y avenida Quito, al sur de la ciudad de Guayaquil, Ecuador, donde juega como local el Club Sport Emelec, equipo de la primera división del fútbol ecuatoriano. Fue el primer estadio construido para pertenecer a un club de fútbol en el Ecuador, aunque sus primeros partidos fueron de béisbol. ',
        textAlign: TextAlign.justify,
      ),
    );
  }
}
